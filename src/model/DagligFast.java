package model;

import java.time.LocalDate;
import java.time.LocalTime;

public class DagligFast extends Ordination {

    private Dosis[] doser = new Dosis[4];

    public DagligFast(LocalDate startDen, LocalDate slutDen) {
        super(startDen, slutDen);
        assert startDen != null && slutDen != null;
    }

    @Override
    public double samletDosis() {
        return antalDage() * doegnDosis();
    }

    @Override
    public double doegnDosis() {
        double dosisPåEtDøgn = 0;
        for (int i = 0; i < doser.length; i++) {
            if (doser[i] != null) {
                dosisPåEtDøgn += doser[i].getAntal();
            }
        }
        return dosisPåEtDøgn;
    }

    @Override
    public String getType() {

        return "Daglig Fast";
    }

    public Dosis[] getDosis() {
        return doser;
    }

    public void createDosis(double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
        if (morgenAntal > 0) {
            doser[0] = new Dosis(LocalTime.of(6, 0), morgenAntal);
        }
        if (middagAntal > 0) {
            doser[1] = new Dosis(LocalTime.of(12, 0), middagAntal);
        }
        if (aftenAntal > 0) {
            doser[2] = new Dosis(LocalTime.of(18, 0), aftenAntal);
        }
        if (natAntal > 0) {
            doser[3] = new Dosis(LocalTime.of(0, 0), natAntal);
        }
    }
}