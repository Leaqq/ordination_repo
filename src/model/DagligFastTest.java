package model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

public class DagligFastTest {

	@Test
	public void samletDosis1() { //Will be stopped in Service

		// Arrange
		DagligFast  df = new DagligFast(LocalDate.of(2018, 02, 03),LocalDate.of(2018, 01, 03));
		df.createDosis(1, 2, 1, 2);
		// Act
		double actualResult = df.doegnDosis()*df.antalDage();
		// Assert
		assertEquals(-180,actualResult,0.001);
	}

	@Test
	public void samletDosis2() {

		// Arrange
		DagligFast  df = new DagligFast(LocalDate.of(2018, 01, 03),LocalDate.of(2018, 01, 04));
		df.createDosis(1, 1, 1, 1);
		// Act
		double actualResult = df.doegnDosis()*df.antalDage();
		// Assert
		assertEquals(8,actualResult,0.001);
	}

	@Test
	public void samletDosis3() {

		// Arrange
		DagligFast  df = new DagligFast(LocalDate.of(2018, 01, 03),LocalDate.of(2019, 01, 03));
		df.createDosis(1, 3, 0, 2);
		// Act
		double actualResult = df.doegnDosis()*df.antalDage();
		// Assert
		assertEquals(2196,actualResult,0.001);
	}
	
	@Test
	public void createDosisTest1() {

		// Arrange
		DagligFast  df = new DagligFast(LocalDate.of(2018, 01, 03),LocalDate.of(2019, 01, 03));
		double morgenAntal = 0.0;
		double middagAntal = 10.0;
		double aftenAntal = 8.0;
		double natAntal = 2.0;
		df.createDosis(morgenAntal,middagAntal,aftenAntal,natAntal);
		// Act
		Dosis[] expected = df.getDosis();
		Dosis[] actual = new Dosis[4];
		actual[0] = new Dosis(LocalTime.of(10, 30), 0.0);
		actual[1] = new Dosis(LocalTime.of(10, 31),10.0);
		actual[2] = new Dosis(LocalTime.of(10, 32),8.0);
		actual[3] = new Dosis(LocalTime.of(10, 33),2.0);
		// Assert
		boolean correct = false;
		if(actual[0].getAntal() == expected[0].getAntal() &&
				actual[1].getAntal() == expected[1].getAntal() &&
				actual[2].getAntal() == expected[2].getAntal() &&
				actual[3].getAntal() == expected[3].getAntal()) {
			correct = true;
		}
		assertEquals(correct,true);
	}
	
	@Test
	public void createDosisTest2() {

		// Arrange
		DagligFast  df = new DagligFast(LocalDate.of(2018, 01, 03),LocalDate.of(2019, 01, 03));
		double morgenAntal = -1.0;
		double middagAntal = -1.0;
		double aftenAntal = -1.0;
		double natAntal = -1.0;
		df.createDosis(morgenAntal,middagAntal,aftenAntal,natAntal);
		// Act
		Dosis[] expected = df.getDosis();
		// Assert
		assertNull(expected[0]);
		assertNull(expected[1]);
		assertNull(expected[2]);
		assertNull(expected[3]);
	}
	
	@Test
	public void createDosisTest3() {

		// Arrange
		DagligFast  df = new DagligFast(LocalDate.of(2018, 01, 03),LocalDate.of(2019, 01, 03));
		double morgenAntal = 1.0;
		double middagAntal = 10.0;
		double aftenAntal = 8.0;
		double natAntal = 2.0;
		df.createDosis(morgenAntal,middagAntal,aftenAntal,natAntal);
		// Act
		Dosis[] expected = df.getDosis();
		Dosis[] actual = new Dosis[4];
		actual[0] = new Dosis(LocalTime.of(10, 30), 1.0);
		actual[1] = new Dosis(LocalTime.of(10, 31),10.0);
		actual[2] = new Dosis(LocalTime.of(10, 32),8.0);
		actual[3] = new Dosis(LocalTime.of(10, 33),2.0);
		// Assert
		boolean correct = false;
		if(actual[0].getAntal() == expected[0].getAntal() &&
				actual[1].getAntal() == expected[1].getAntal() &&
				actual[2].getAntal() == expected[2].getAntal() &&
				actual[3].getAntal() == expected[3].getAntal()) {
			correct = true;
		}
		assertEquals(correct,true);
	}
	
	@Test
	public void createDosisTest4() {

		// Arrange
		DagligFast  df = new DagligFast(LocalDate.of(2018, 01, 03),LocalDate.of(2019, 01, 03));
		double morgenAntal = 1.0;
		double middagAntal = 3.0;
		double aftenAntal = -1.0;
		double natAntal = 2.0;
		df.createDosis(morgenAntal,middagAntal,aftenAntal,natAntal);
		// Act
		Dosis[] expected = df.getDosis();
		Dosis[] actual = new Dosis[4];
		actual[0] = new Dosis(LocalTime.of(10, 30), 1.0);
		actual[1] = new Dosis(LocalTime.of(10, 31),3.0);
		actual[3] = new Dosis(LocalTime.of(10, 33),2.0);
		// Assert
		boolean correct = false;
		if(actual[0].getAntal() == expected[0].getAntal() &&
				actual[1].getAntal() == expected[1].getAntal() &&
				actual[3].getAntal() == expected[3].getAntal()) {
			correct = true;
		}
		assertEquals(correct,true);
		assertNull(expected[2]);
	}
}

