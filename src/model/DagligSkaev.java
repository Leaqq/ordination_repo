package model;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

public class DagligSkaev extends Ordination {

    private ArrayList<Dosis> doser = new ArrayList<>();

    public DagligSkaev(LocalDate startDen, LocalDate slutDen) {
        super(startDen, slutDen);
    }

    /** Pre: antal >= 0. */
    public void opretDosis(LocalTime tid, double antal) {
        assert antal >= 0;
        Dosis dosis = new Dosis(tid, antal);
        doser.add(dosis);
    }

    @Override
    public double samletDosis() {
        return doegnDosis() * antalDage();

    }

    @Override
    public double doegnDosis() {
        int sum = 0;
        for (Dosis dosis : doser) {
            sum += dosis.getAntal();
        }
        return sum;
    }

    @Override
    public String getType() {
        return "Daglig Skæv";
    }

    public void addDosis(Dosis dosis) {
        doser.add(dosis);
    }

    public void removeDosis(Dosis dosis) {
        doser.remove(dosis);
    }

    public ArrayList<Dosis> getDoser() {
        return doser;
    }

}
