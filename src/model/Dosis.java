package model;

import java.time.LocalTime;

public class Dosis {
    private final LocalTime tid;
    private final double antal;
    // Negativt antal betyder, at antal ikke er sat.

    public Dosis(LocalTime tid, double antal) {
        assert tid != null;
        this.tid = tid;
        this.antal = antal;
    }

    public double getAntal() {
        return antal;
    }

    public LocalTime getTid() {
        return tid;
    }

    @Override
    public String toString() {
        return "Kl: " + tid + "   antal:  " + antal;
    }
}
 