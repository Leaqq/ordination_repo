package model;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class DagligSkaevTest {
    private DagligSkaev ds;
    private LocalTime time1, time2, time3, time4, time5, time6, time7, time8, time9;
    private Dosis dose1, dose2, dose3, dose4, dose5, dose6, dose7, dose8, dose9, dose10, dose11, dose12, dose13, dose14,
            dose15;
    private List<Dosis> arrDoser = new ArrayList<>();

    @Before
    public void setup() {
        // this.dagligskaev = new DagligSkaev(null, null);
        time1 = LocalTime.parse("09:00");
        time2 = LocalTime.parse("10:10");
        time3 = LocalTime.parse("23:59");
        time4 = LocalTime.parse("11:59");
        time5 = LocalTime.parse("12:15");
        time6 = LocalTime.parse("16:59");
        time7 = LocalTime.parse("19:00");
        time8 = LocalTime.parse("23:59");
        time9 = LocalTime.parse("00:01");
        dose1 = new Dosis(time1, 10);
        dose2 = new Dosis(time1, 0);
        dose3 = new Dosis(time1, 3);
        dose4 = new Dosis(time4, 2);
        dose5 = new Dosis(time5, 3);
        dose6 = new Dosis(time6, 4);
        dose7 = new Dosis(time7, -4);
        dose8 = new Dosis(time1, 1);
        dose9 = new Dosis(time1, 2);
        dose10 = new Dosis(time6, 0);
        dose11 = new Dosis(time7, 4);
        dose12 = new Dosis(time9, 3);
        dose13 = new Dosis(time5, 5);
        dose14 = new Dosis(time6, 5);
        dose15 = new Dosis(time8, 5);

    }

    @Test
    public void testOpretDosis1() {
        ds = new DagligSkaev(LocalDate.parse("2018-02-03"), LocalDate.parse("2018-01-03"));
        ds.opretDosis(time1, 0);
        assertEquals(LocalTime.parse("09:00"), ds.getDoser().get(0).getTid());
        assertEquals(0, ds.getDoser().get(0).getAntal(), 0.001);

    }

    @Test
    public void testOpretDosis2() {
        ds = new DagligSkaev(LocalDate.parse("2018-03-01"), LocalDate.parse("2018-04-01"));
        ds.opretDosis(time2, 1);
        assertEquals(LocalTime.parse("10:10"), ds.getDoser().get(0).getTid());
        assertEquals(1, ds.getDoser().get(0).getAntal(), 0.001);
    }

    @Test
    public void testOpretDosis3() {
        ds = new DagligSkaev(LocalDate.parse("2018-03-01"), LocalDate.parse("2019-03-01"));
        ds.opretDosis(time3, 10);
        assertEquals(LocalTime.parse("23:59"), ds.getDoser().get(0).getTid());
        assertEquals(10, ds.getDoser().get(0).getAntal(), 0.001);

    }

    @Test
    public void testSamletDosis1() {
        ds = new DagligSkaev(LocalDate.parse("2018-03-01"), LocalDate.parse("2019-03-01"));
        ds.addDosis(dose1);
        assertEquals(3660, ds.samletDosis(), 0.001);
    }

    @Test
    public void testSamletDosis2() {
        ds = new DagligSkaev(LocalDate.parse("2018-03-01"), LocalDate.parse("2018-03-01"));
        ds.addDosis(dose2);
        assertEquals(0, ds.samletDosis(), 0.001);
    }

    // @Test(expected = IllegalArgumentException.class)
    // public void testSamletDosis3() {
    // ds = new DagligSkaev(LocalDate.parse("2018-03-02"),
    // LocalDate.parse("2018-03-01"));
    // ds.addDosis(dose3);
    // }

    @Test
    public void testSamletDosis4() {
        ds = new DagligSkaev(LocalDate.parse("2018-03-01"), LocalDate.parse("2018-03-04"));
        ds.addDosis(dose3);
        assertEquals(12, ds.samletDosis(), 0.001);
    }

    // @Test(expected = IllegalArgumentException.class)
    // public void testDøgnDosis1() {
    // ds = new DagligSkaev(LocalDate.parse("2018-03-01"),
    // LocalDate.parse("2019-03-01"));
    // }
    //
    // @Test(expected = IllegalArgumentException.class)
    // public void testDøgnDosis2() {
    // ds = new DagligSkaev(LocalDate.parse("2018-03-01"),
    // LocalDate.parse("2019-03-01"));
    // ds.addDosis(dose8);
    // ds.addDosis(dose4);
    // ds.addDosis(dose5);
    // ds.addDosis(dose6);
    // ds.addDosis(dose7);
    // }

    @Test
    public void testDøgnDosis3() {
        ds = new DagligSkaev(LocalDate.parse("2018-03-01"), LocalDate.parse("2019-03-01"));
        ds.addDosis(dose12);
        ds.addDosis(dose4);
        ds.addDosis(dose5);
        ds.addDosis(dose10);
        ds.addDosis(dose11);
        double res = ds.doegnDosis();
        assertEquals(12, res, 0.001);
    }

    @Test
    public void testDøgnDosis4() {
        ds = new DagligSkaev(LocalDate.parse("2018-03-01"), LocalDate.parse("2019-03-01"));
        ds.addDosis(dose9);
        ds.addDosis(dose4);
        ds.addDosis(dose13);
        ds.addDosis(dose14);
        ds.addDosis(dose15);
        double res = ds.doegnDosis();
        assertEquals(19, res, 0.001);
    }

}
