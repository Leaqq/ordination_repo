package model;

public class Laegemiddel {
    private final String navn;
    // faktor der anvendes hvis patient vejer < 25 kg
    private final double enhedPrKgPrDoegnLet; // >= 0
    // faktor der anvendes hvis 25 kg <= patient vægt <= 120 kg
    private final double enhedPrKgPrDoegnNormal; // >= 0
    // faktor der anvendes hvis patient vægt > 120 kg
    private final double enhedPrKgPrDoegnTung; // >= 0
    private final String enhed; // ikke tom

    /**
     * Pre: navn ikke tomt, enhed ikke tomt, Pre: enhedPrKgDoegnLet >= 0,
     * enhedPrKgDoegnNormal >= 0, enhedPrKgDoegnTung >= 0
     */
    public Laegemiddel(String navn, double enhedPrKgPrDoegnLet, double enhedPrKgPrDoegnNormal,
            double enhedPrKgPrDoegnTung, String enhed) {
        assert !navn.isEmpty() && !enhed.isEmpty();
        assert enhedPrKgPrDoegnLet >= 0 && enhedPrKgPrDoegnNormal >= 0 && enhedPrKgPrDoegnTung >= 0;
        this.navn = navn;
        this.enhedPrKgPrDoegnLet = enhedPrKgPrDoegnLet;
        this.enhedPrKgPrDoegnNormal = enhedPrKgPrDoegnNormal;
        this.enhedPrKgPrDoegnTung = enhedPrKgPrDoegnTung;
        this.enhed = enhed;
    }

    public String getEnhed() {
        return enhed;
    }

    public String getNavn() {
        return navn;
    }

    public double getEnhedPrKgPrDoegnLet() {
        return enhedPrKgPrDoegnLet;
    }

    public double getEnhedPrKgPrDoegnNormal() {
        return enhedPrKgPrDoegnNormal;
    }

    public double getEnhedPrKgPrDoegnTung() {
        return enhedPrKgPrDoegnTung;
    }

    @Override
    public String toString() {
        return navn;
    }
}
 