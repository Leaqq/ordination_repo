package model;

import java.time.LocalDate;
import java.util.ArrayList;
import static java.time.temporal.ChronoUnit.DAYS;

public class PN extends Ordination {

    private final double antalEnheder;
    private ArrayList<LocalDate> dosisGivetDen = new ArrayList<>();

    public PN(LocalDate startDen, LocalDate slutDen, double antalEnheder) {
        super(startDen, slutDen);
        this.antalEnheder = antalEnheder;
    }

    /**
     * Registrerer at der er givet en dosis paa dagen givesDen. Returnerer true,
     * hvis givesDen er inden for ordinationens gyldighedsperiode, og datoen huskes.
     * Returnerer false ellers og datoen givesDen ignoreres.
     */
    public boolean givDosis(LocalDate givesDen) {
        assert givesDen != null;
        if (givesDen.isAfter(getStartDen()) && givesDen.isBefore(getSlutDen())) {
            dosisGivetDen.add(givesDen);
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returnerer antal gange ordinationen er anvendt.
     */
    public int getAntalGangeGivet() {
        return dosisGivetDen.size();
    }

    public double getAntalEnheder() {
        return antalEnheder;
    }

    @Override
    public double samletDosis() {
        return dosisGivetDen.size() * antalEnheder;

    }

    @Override
    public double doegnDosis() {
        if (dosisGivetDen.size() > 0) {
            return (dosisGivetDen.size() * antalEnheder)
                    / DAYS.between(dosisGivetDen.get(0), dosisGivetDen.get(dosisGivetDen.size()));
        } else {
            return 0;
        }

    }

    @Override
    public String getType() {
        return "PN";
    }
}
