package service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import model.DagligFast;
import model.DagligSkaev;
import model.Laegemiddel;
import model.Ordination;
import model.PN;
import model.Patient;
import storage.Storage;

public class Service {
    private static Storage storage;

    public Service(Storage storage) {
        Service.storage = storage;
    }

    public Service() {
        //
    }

    /**
     * Opretter og returnerer en PN ordination. Hvis startDato er efter slutDato
     * kastes en IllegalArgumentException, og ordinationen oprettes ikke. Pre: antal
     * >= 0.
     */
    public PN opretPNOrdination(LocalDate startDen, LocalDate slutDen, Patient patient, Laegemiddel laegemiddel,
            double antal) {
        assert antal > 0;
        if (startDen.isAfter(slutDen)) {
            throw new IllegalArgumentException("Startdato er efter Slutdato");
        }
        PN pn = new PN(startDen, slutDen, antal);
        pn.setLaegemiddel(laegemiddel);
        patient.addOrdination(pn);
        return pn;

    }

    /**
     * Opretter og returnerer en DagligSkæv ordination. Hvis startDato er efter
     * slutDato kastes en IllegalArgumentException, og ordinationen oprettes ikke.
     * Hvis antallet af elementer i klokkeSlet og antalEnheder er forskellige kastes
     * en IllegalArgumentException. Pre: Alle tal i antalEnheder er >= 0.
     */
    public DagligSkaev opretDagligSkaevOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
            Laegemiddel laegemiddel, LocalTime[] klokkeSlet, double[] antalEnheder) {
        if (startDen.getDayOfYear() > slutDen.getDayOfYear()) {
            throw new IllegalArgumentException("Start datoen skal være før slut datoen");
        }
        if (!(klokkeSlet.length == antalEnheder.length)) {
            throw new IllegalArgumentException("Antal enheder skal være ens med klokkeslet");
        }
        DagligSkaev ds = new DagligSkaev(startDen, slutDen);
        patient.addOrdination(ds);
        ds.setLaegemiddel(laegemiddel);
        for (int i = 0; i < klokkeSlet.length; i++) {
            ds.opretDosis(klokkeSlet[i], antalEnheder[i]);
        }
        return ds;
    }

    /**
     * Opretter og returnerer en DagligFast ordination. Hvis startDato er efter
     * slutDato kastes en IllegalArgumentException, og ordinationen oprettes ikke.
     * Pre: Alle antal er >= 0 eller -1 (dvs. ikke sat).
     */
    public DagligFast opretDagligFastOrdination(LocalDate startDen, LocalDate slutDen, Patient patient,
            Laegemiddel laegemiddel, double morgenAntal, double middagAntal, double aftenAntal, double natAntal) {
        if (startDen.isAfter(slutDen)) {
            throw new IllegalArgumentException("Startdato er efter Slutdato");
        }
        DagligFast df = new DagligFast(startDen, slutDen);
        df.setLaegemiddel(laegemiddel);
        df.createDosis(morgenAntal, middagAntal, aftenAntal, natAntal);
        patient.addOrdination(df);
        return df;
    }

    /**
     * En dato for hvornår ordinationen anvendes tilføjes ordinationen. Hvis datoen
     * ikke er indenfor ordinationens gyldighedsperiode kastes en
     * IllegalArgumentException.
     */
    public void ordinationPNAnvendt(PN pn, LocalDate dato) {
        if (pn.givDosis(dato)) {
        } else {
            throw new IllegalArgumentException("Ikke længere gyldig");
        }
    }

    /**
     * . Den anbefalede dosis for den pågældende patient (afhænger af patientens
     * vægt).
     */
    public double anbefaletDosisPrDoegn(Patient patient, Laegemiddel laegemiddel) {
        double result;
        if (patient.getVaegt() < 25) {
            result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnLet();
        } else if (patient.getVaegt() > 120) {
            result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnTung();
        } else {
            result = patient.getVaegt() * laegemiddel.getEnhedPrKgPrDoegnNormal();
        }
        return result;
    }

    /**
     * For et givent vægtinterval og et givent lægemiddel returneres antallet af
     * ordinationer. Pre: 0 <= vægtStart <= vægtSlut.
     */
    public int antalOrdinationerPrVægtPrLægemiddel(double vaegtStart, double vaegtSlut, Laegemiddel laegemiddel) {
        int antal = 0;
        for (Patient p : storage.getAllPatienter()) {
            for (Ordination o : p.getOrdinationer()) {
                if (o.getLaegemiddel() == laegemiddel && p.getVaegt() >= vaegtStart && p.getVaegt() <= vaegtSlut) {
                    antal++;
                }
            }
        }
        return antal;
    }

    public List<Patient> getAllPatienter() {
        return storage.getAllPatienter();
    }

    public List<Laegemiddel> getAllLaegemidler() {
        return storage.getAllLaegemidler();
    }

    /**
     * Pre: navn ikke tomt, cpr ikke tomt, vaegt >= 0.
     */
    public Patient opretPatient(String cpr, String navn, double vaegt) {
        Patient p = new Patient(cpr, navn, vaegt);
        storage.addPatient(p);
        return p;
    }

    /**
     * Pre: navn ikke tomt, enhed ikke tomt.<br/>
     * Pre: enhedPrKgDoegnLet >= 0, enhedPrKgDoegnnormal >= 0, enhedPrKgDoegnTung >=
     * 0.
     */
    public Laegemiddel opretLaegemiddel(String navn, double enhedPrKgPrDoegnLet, double enhedPrKgPrDoegnNormal,
            double enhedPrKgPrDoegnTung, String enhed) {
        assert !navn.isEmpty() && !enhed.isEmpty();
        assert enhedPrKgPrDoegnLet >= 0 && enhedPrKgPrDoegnNormal >= 0 && enhedPrKgPrDoegnTung >= 0;
        Laegemiddel lm = new Laegemiddel(navn, enhedPrKgPrDoegnLet, enhedPrKgPrDoegnNormal, enhedPrKgPrDoegnTung,
                enhed);
        storage.addLaegemiddel(lm);
        return lm;
    }

    public void createSomeObjects() {
        opretPatient("121256-0512", "Jane Jensen", 63.4);
        opretPatient("070985-1153", "Finn Madsen", 83.2);
        opretPatient("050972-1233", "Hans Jørgensen", 89.4);
        opretPatient("011064-1522", "Ulla Nielsen", 59.9);
        opretPatient("090149-2529", "Ib Hansen", 87.7);

        opretLaegemiddel("Acetylsalicylsyre", 0.1, 0.15, 0.16, "Styk");
        opretLaegemiddel("Paracetamol", 1, 1.5, 2, "Ml");
        opretLaegemiddel("Fucidin", 0.025, 0.025, 0.025, "Styk");
        opretLaegemiddel("Methotrexat", 0.01, 0.015, 0.02, "Styk");

        opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), storage.getAllPatienter().get(0),
                storage.getAllLaegemidler().get(1), 123);

        opretPNOrdination(LocalDate.of(2015, 2, 12), LocalDate.of(2015, 2, 14), storage.getAllPatienter().get(0),
                storage.getAllLaegemidler().get(0), 3);

        opretPNOrdination(LocalDate.of(2015, 1, 20), LocalDate.of(2015, 1, 25), storage.getAllPatienter().get(3),
                storage.getAllLaegemidler().get(2), 5);

        opretPNOrdination(LocalDate.of(2015, 1, 1), LocalDate.of(2015, 1, 12), storage.getAllPatienter().get(0),
                storage.getAllLaegemidler().get(1), 123);

        opretDagligFastOrdination(LocalDate.of(2015, 1, 10), LocalDate.of(2015, 1, 12),
                storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(1), 2, -1, 1, -1);

        LocalTime[] kl = { LocalTime.of(12, 0), LocalTime.of(12, 40), LocalTime.of(16, 0), LocalTime.of(18, 45) };
        double[] an = { 0.5, 1, 2.5, 3 };

        opretDagligSkaevOrdination(LocalDate.of(2015, 1, 23), LocalDate.of(2015, 1, 24),
                storage.getAllPatienter().get(1), storage.getAllLaegemidler().get(2), kl, an);
    }
}
