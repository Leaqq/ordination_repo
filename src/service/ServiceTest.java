package service;

import static org.junit.Assert.*;

import java.time.LocalDate;
import java.time.LocalTime;

import org.junit.Test;

import model.DagligFast;
import model.DagligSkaev;
import model.Laegemiddel;
import model.PN;
import model.Patient;
import service.Service;
import storage.Storage;

public class ServiceTest {

    private Storage sr = new Storage();
    private Service sv = new Service(sr);
    private LocalTime[] tid;
    private double[] enheder;

    @Test
    public void opretPNOrdinationTest1() {
        Patient p = sv.opretPatient("123456-1234", "Jane Jensen", 60);
        Laegemiddel lm = sv.opretLaegemiddel("Methotrexat", 25, 75, 100, "Ved ikke hvad enhed er");
        sr.addPatient(p);
        sr.addLaegemiddel(lm);
        PN pn = sv.opretPNOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), p, lm, 5);
        LocalDate startDate = LocalDate.of(2018, 01, 01);
        LocalDate endDate = LocalDate.of(2018, 01, 02);
        int antal = 5;

        assertEquals(startDate, pn.getStartDen());
        assertEquals(endDate, pn.getSlutDen());
        assertEquals(antal, pn.getAntalEnheder(), 0.001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void opretPNOrdinationTest2() {
        Laegemiddel lm = sv.opretLaegemiddel("Fucidin", 35, 55, 80, "Ved ikke hvad enhed er");
        Patient p1 = sv.opretPatient("123456-1234", "Jane Jensen", 60);
        sv.opretPNOrdination(LocalDate.of(2018, 01, 02), LocalDate.of(2018, 01, 01), p1, lm, 10);
    }

    @Test(expected = IllegalArgumentException.class)

    public void opretDagligSkaevOrdination1() {

        tid = new LocalTime[] { LocalTime.of(10, 30), LocalTime.of(12, 30), LocalTime.of(15, 30),
                LocalTime.of(18, 30) };

        enheder = new double[] { 2.0, 3.0, 1.0, 4.0 };

        Patient p = sv.opretPatient("123456-1234", "Jane Jensen", 60);

        Laegemiddel lm = sv.opretLaegemiddel("Paracetamol", 25, 75, 100, "Ved ikke hvad det er");

        DagligSkaev opretDSO = sv.opretDagligSkaevOrdination(LocalDate.of(2018, 01, 03), LocalDate.of(2018, 01, 02), p,
                lm, tid, enheder);

    }

    @Test
    public void opretDagligSkaevOrdination2() {

        tid = new LocalTime[] { LocalTime.of(10, 30), LocalTime.of(12, 30), LocalTime.of(15, 30),
                LocalTime.of(18, 30) };

        enheder = new double[] { 2.0, 3.0, 1.0, 4.0 };

        Patient p = sv.opretPatient("123456-1234", "Jane Jensen", 60);

        Laegemiddel lm = sv.opretLaegemiddel("Paracetamol", 25, 75, 100, "Ved ikke hvad det er");

        DagligSkaev opretDSO = sv.opretDagligSkaevOrdination(LocalDate.of(2018, 01, 01), LocalDate.of(2018, 01, 02), p,
                lm, tid, enheder);

        LocalDate startDate = LocalDate.of(2018, 01, 01);

        LocalDate endDate = LocalDate.of(2018, 01, 02);

        LocalTime kl0 = opretDSO.getDoser().get(0).getTid();

        LocalTime kl1 = opretDSO.getDoser().get(1).getTid();

        LocalTime kl2 = opretDSO.getDoser().get(2).getTid();

        LocalTime kl3 = opretDSO.getDoser().get(3).getTid();

        double an0 = opretDSO.getDoser().get(0).getAntal();

        double an1 = opretDSO.getDoser().get(1).getAntal();

        double an2 = opretDSO.getDoser().get(2).getAntal();

        double an3 = opretDSO.getDoser().get(3).getAntal();

        assertEquals(startDate, opretDSO.getStartDen());

        assertEquals(endDate, opretDSO.getSlutDen());

        assertEquals(LocalTime.of(10, 30), kl0);

        assertEquals(LocalTime.of(12, 30), kl1);

        assertEquals(LocalTime.of(15, 30), kl2);

        assertEquals(LocalTime.of(18, 30), kl3);

        assertEquals(2.0, an0, 0.001);

        assertEquals(3.0, an1, 0.001);

        assertEquals(1.0, an2, 0.001);

        assertEquals(4.0, an3, 0.001);

        assertEquals(true, p.getOrdinationer().contains(opretDSO));

        assertEquals(lm, opretDSO.getLaegemiddel());

    }

    @Test(expected = IllegalArgumentException.class)
    public void opretDagligFastOrdinationtest1() {
        Patient p = sv.opretPatient("050972-1233", "Hans Jørgensen", 89.4);
        Laegemiddel lm = sv.opretLaegemiddel("Paracetamol", 25, 75, 100, "Enhed?");
        sr.addPatient(p);
        sr.addLaegemiddel(lm);
        DagligFast df = sv.opretDagligFastOrdination(LocalDate.of(2018, 03, 11), LocalDate.of(2018, 03, 10), p, lm, 2,
                1, 0, 10.5);

    }

    @Test
    public void opretDagligFastOrdinationtest2() {
        Patient p = sv.opretPatient("090149-2529", "Ib Hansen", 87.7);
        Laegemiddel lm = sv.opretLaegemiddel("Fucidin", 25, 75, 100, "Enhed?");
        sr.addPatient(p);
        sr.addLaegemiddel(lm);
        DagligFast df = sv.opretDagligFastOrdination(LocalDate.of(2018, 04, 02), LocalDate.of(2020, 03, 10), p, lm, 15,
                3.8, 9.1, 10.5);

        assertEquals(LocalDate.of(2018, 04, 02), df.getStartDen());
        assertEquals(LocalDate.of(2020, 03, 10), df.getSlutDen());
        assertEquals(38.4, df.doegnDosis(), 0.001);
    }

    @Test
    public void ordinationPNAnvendt1() {
        Patient p = sv.opretPatient("123456-1234", "Jane Jensen", 60);
        Laegemiddel lm = sv.opretLaegemiddel("Methotrexat", 25, 75, 100, "Ved ikke hvad enhed er");
        PN pn = sv.opretPNOrdination(LocalDate.parse("2018-12-12"), LocalDate.parse("2019-01-01"), p, lm, 5);
        pn.givDosis(LocalDate.parse("2018-12-24"));
        assertEquals(1, pn.getAntalGangeGivet());
    }

    @Test
    public void ordinationPNAnvendt2() {
        Patient p = sv.opretPatient("123456-1234", "Jane Jensen", 60);
        Laegemiddel lm = sv.opretLaegemiddel("Methotrexat", 25, 75, 100, "Ved ikke hvad enhed er");
        PN pn = sv.opretPNOrdination(LocalDate.parse("2018-12-12"), LocalDate.parse("2019-01-01"), p, lm, 5);
        pn.givDosis(LocalDate.parse("2019-12-24"));
        assertEquals(0, pn.getAntalGangeGivet());
    }

    @Test

    public void antalOrdinationerPrVægtPrLægeMiddel1() {

        Laegemiddel lm = sv.opretLaegemiddel("Paracetamol", 25, 75, 100, "Enhed?");

        Patient p = sv.opretPatient("050972-1233", "Hans Jørgensen", 89.4);

        // opret PNordinationer

        sv.opretPNOrdination(LocalDate.of(2018, 04, 02), LocalDate.of(2020, 03, 10), p, lm, 10);

        sv.opretPNOrdination(LocalDate.of(2018, 04, 02), LocalDate.of(2020, 03, 10), p, lm, 10);

        sv.opretPNOrdination(LocalDate.of(2018, 04, 02), LocalDate.of(2020, 03, 10), p, lm, 10);

        int antal = sv.antalOrdinationerPrVægtPrLægemiddel(10, 100, lm);

        assertEquals(3, antal, 0.01);

    }

    @Test

    public void antalOrdinationerPrVægtPrLægeMiddel2() {

        Laegemiddel lm = sv.opretLaegemiddel("Methotrexat", 25, 75, 100, "Enhed?");

        Patient p = sv.opretPatient("050972-1233", "Hans Jørgensen", 89.4);

        // opret PNordinationer

        sv.opretPNOrdination(LocalDate.of(2018, 04, 02), LocalDate.of(2020, 03, 10), p, lm, 10);

        int antal = sv.antalOrdinationerPrVægtPrLægemiddel(-1, 100, lm);

        assertEquals(1, antal, 0.01);

    }

    @Test

    public void antalOrdinationerPrVægtPrLægeMiddel3() {

        Laegemiddel lm = sv.opretLaegemiddel("Methotrexat", 25, 75, 100, "Enhed?");

        Patient p = sv.opretPatient("050972-1233", "Hans Jørgensen", 89.4);

        // opret PNordinationer

        sv.opretPNOrdination(LocalDate.of(2018, 04, 02), LocalDate.of(2020, 03, 10), p, lm, 10);

        int antal = sv.antalOrdinationerPrVægtPrLægemiddel(100, 10, lm);

        assertEquals(0, antal, 0.01);

    }

    @Test

    public void antalOrdinationerPrVægtPrLægeMiddel4() {

        Laegemiddel lm = sv.opretLaegemiddel("Paracetamol", 25, 75, 100, "Enhed?");

        Patient p = sv.opretPatient("050972-1233", "Hans Jørgensen", 89.4);

        // opret PNordinationer

        sv.opretPNOrdination(LocalDate.of(2018, 04, 02), LocalDate.of(2020, 03, 10), p, lm, 10);

        int antal = sv.antalOrdinationerPrVægtPrLægemiddel(-1, -10, lm);

        assertEquals(0, antal, 0.01);

    }

}
