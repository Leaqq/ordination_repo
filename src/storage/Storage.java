package storage;

import java.util.ArrayList;
import java.util.List;

import model.Laegemiddel;
import model.Patient;

public class Storage {
    private final List<Patient> patienter = new ArrayList<>();
    private final List<Laegemiddel> laegemidler = new ArrayList<>();

    public List<Patient> getAllPatienter() {
        return new ArrayList<Patient>(patienter);
    }

    public void addPatient(Patient patient) {
        patienter.add(patient);
    }

    public List<Laegemiddel> getAllLaegemidler() {
        return new ArrayList<Laegemiddel>(laegemidler);
    }

    public void addLaegemiddel(Laegemiddel laegemiddel) {
        laegemidler.add(laegemiddel);
    }
}
 