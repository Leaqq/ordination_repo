package gui;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;

public class DagligFastPane extends GridPane {
    private TextField txtMorgen = new TextField();
    private TextField txtMiddag = new TextField();
    private TextField txtAften = new TextField();
    private TextField txtNat = new TextField();

    public DagligFastPane(int maxWidth) {
        this.setHgap(20);
        this.setVgap(10);
        this.setGridLinesVisible(false);
        this.setMaxWidth(maxWidth);

        this.add(new Label("Morgen"), 0, 0);
        this.add(new Label("Middag"), 1, 0);
        this.add(new Label("Aften"), 2, 0);
        this.add(new Label("Nat"), 3, 0);
        this.add(txtMorgen, 0, 1);
        this.add(txtMiddag, 1, 1);
        this.add(txtAften, 2, 1);
        this.add(txtNat, 3, 1);
    }

    public void setMorgen(String morgen) {
        txtMorgen.setText(morgen);
    }

    public void setMiddag(String middag) {
        txtMiddag.setText(middag);
    }

    public void setAften(String aften) {
        txtAften.setText(aften);
    }

    public void setNat(String nat) {
        txtNat.setText(nat);
    }

    public Double getMorgen() {
        return this.parseTextField(txtMorgen);
    }

    public Double getMiddag() {
        return this.parseTextField(txtMiddag);
    }

    public Double getAften() {
        return this.parseTextField(txtAften);
    }

    public Double getNat() {
        return this.parseTextField(txtNat);
    }

    private Double parseTextField(TextField textField) {
        if (textField.getText().isEmpty())
            return null;
        else
            return Double.parseDouble(textField.getText());
    }

    public void makeReadOnly() {
        txtMorgen.setEditable(false);
        txtMiddag.setEditable(false);
        txtAften.setEditable(false);
        txtNat.setEditable(false);
    }
}
 